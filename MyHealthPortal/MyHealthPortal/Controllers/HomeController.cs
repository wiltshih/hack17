﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyHealthPortal.Controllers
{
	public class HomeController : Controller
	{
		public ActionResult Index()
		{
			return View();
		}

		public ActionResult Select(Guid? userId = null)
		{
			//.ViewBag.User = userId.Value;

			return View();
		}

		public ActionResult Main()
		{
			ViewBag.Message = "Your contact page.";

			return View();
		}
	}
}